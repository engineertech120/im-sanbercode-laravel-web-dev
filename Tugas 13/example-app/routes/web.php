<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\indexcontroller;
use App\Http\Controllers\formcontroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [indexcontroller::class, 'utama']);
Route::get('/biodata', [formcontroller::class, 'bio']);
Route::post('/welcome', [formcontroller::class, 'send']);

Route::get('/master', function(){
    return view ('layout.master');
    });

    Route::get('/data-table', function(){
        return view ('page.data-table');
        });
        Route::get('/table', function(){
            return view ('page.table');
            });
