<?php
require_once('Animal.php');
require_once ('Frog.php');
require_once ('Ape.php');

$Animal = new Animal("Shaun");

echo "Name : " . $Animal->name . "<br>";
echo "Legs : " . $Animal->legs . "<br>";
echo "Cold Blooded : " . $Animal->coldblooded . "<br><br>";

$Frog = new Frog("buduk");
echo "Name : " . $Frog->name . "<br>";
echo "Jump : " . $Frog->legs . "<br>";
echo "Cold Blooded : " . $Frog->coldblooded . "<br>";
echo $Frog->jump();

$Ape = new Ape("Kera sakti");
echo "Name : " . $Ape->name . "<br>";
echo "Jump : " . $Ape->legs . "<br>";
echo "Cold Blooded : " . $Ape->coldblooded . "<br>";
echo $Ape->yell();

?>