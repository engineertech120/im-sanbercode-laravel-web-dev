<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf   
        <label>First name:</label> <br><br>
        <input type="text" name="fname"> <br> <br>
        <label>Last name:</label> <br><br>
        <input type="text" name="lname"> <br> <br>
       
        <label>Gender</label> <br><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
       
        <label>Nationality:</label> <br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Germany">Malaysia</option>
            <option value="Amerika Serikat">Malaysia</option>
        </select><br><br>

        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="Language Speaker">Bahasa Indonesia<br>
        <input type="checkbox" name="Language Speaker">English<br>
        <input type="checkbox" name="Language Speaker">Other<br><br>
   
        <label>Bio:</label> <br><br>
        <textarea name="message" rows="10" cols="30"></textarea> <br><br>
        
        <input type="submit" value="Sign Up"><br>
    </form>
</body>
</html>